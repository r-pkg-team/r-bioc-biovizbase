r-bioc-biovizbase (1.54.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 14 Jan 2025 08:31:54 +0100

r-bioc-biovizbase (1.54.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 30 Nov 2024 17:18:27 +0100

r-bioc-biovizbase (1.52.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:34:55 +0200

r-bioc-biovizbase (1.52.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/control: bump minimum versions of r-bioc-* packages to bioc-3.19+
    versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 30 Jul 2024 17:07:38 +0200

r-bioc-biovizbase (1.52.0-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 10 Jul 2024 01:37:00 +0200

r-bioc-biovizbase (1.50.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2023 11:17:38 +0100

r-bioc-biovizbase (1.48.0-1) unstable; urgency=medium

  * Provide autopkgtest-pkg-r.conf to make sure runit will be found
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 18:09:15 +0200

r-bioc-biovizbase (1.46.0-1) unstable; urgency=medium

  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 22 Nov 2022 07:19:10 +0100

r-bioc-biovizbase (1.44.0-1) unstable; urgency=medium

  * Drop unused lintian-override
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 May 2022 06:47:13 +0200

r-bioc-biovizbase (1.42.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 26 Nov 2021 13:48:02 +0100

r-bioc-biovizbase (1.40.0-2) unstable; urgency=medium

  * Disable reprotest
  * Do not run autodep8 since it needs unpackaged data

 -- Andreas Tille <tille@debian.org>  Tue, 14 Sep 2021 12:25:47 +0200

r-bioc-biovizbase (1.40.0-1) unstable; urgency=medium

  * No need to use vignette for autopkgtest since real test is provided
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * lintian-override: No need to link against libc

 -- Andreas Tille <tille@debian.org>  Tue, 14 Sep 2021 08:46:41 +0200

r-bioc-biovizbase (1.38.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * No tab in license text (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 03 Nov 2020 15:47:41 +0100

r-bioc-biovizbase (1.36.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Fri, 22 May 2020 07:56:34 +0200

r-bioc-biovizbase (1.34.1-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix autopkgtest.
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sun, 15 Mar 2020 15:31:34 +0100

r-bioc-biovizbase (1.34.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 07 Dec 2019 12:49:34 +0100

r-bioc-biovizbase (1.34.0-1) unstable; urgency=medium

  * New upstream version
  * Fixed debian/watch for BioConductor
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * debian/copyright: Use spaces rather than tabs in continuation lines.

 -- Andreas Tille <tille@debian.org>  Wed, 13 Nov 2019 09:42:19 +0100

r-bioc-biovizbase (1.32.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * rename debian/tests/control.autodep8 to debian/tests/control

 -- Andreas Tille <tille@debian.org>  Fri, 19 Jul 2019 11:33:07 +0200

r-bioc-biovizbase (1.30.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 14 Dec 2018 10:55:18 +0100

r-bioc-biovizbase (1.30.0-2) unstable; urgency=medium

  * Team upload.
  * Do not call sessionInfo() while building vignettes,
    it fails on low-memory autopkgtesters.

 -- Dylan Aïssi <daissi@debian.org>  Thu, 06 Dec 2018 07:57:48 +0100

r-bioc-biovizbase (1.30.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Mon, 12 Nov 2018 22:05:11 +0100

r-bioc-biovizbase (1.28.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Tue, 28 Aug 2018 16:54:03 +0200

r-bioc-biovizbase (1.28.1-1) unstable; urgency=medium

  * Remove now unneeded Build-Depends r-cran-foreign and r-cran-nnet
    (see #900974)
  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jul 2018 11:36:11 +0200

r-bioc-biovizbase (1.28.0-2) unstable; urgency=medium

  * Force r-cran-foreign and r-cran-nnet also in Depends since it is
    obviously needed - at least the issue re-appears when trying to
    build r-bioc-gviz and probably also in autopkgtest

 -- Andreas Tille <tille@debian.org>  Thu, 07 Jun 2018 09:58:53 +0200

r-bioc-biovizbase (1.28.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 11
  * dh-update-R to update Build-Depends
  * Re-add Build-Depends: r-cran-foreign, r-cran-nnet
    which was erroneously removed by dh-update-R
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * Fix Maintainer and Vcs-fields
  * Rebuild for R 3.5 transition

 -- Andreas Tille <tille@debian.org>  Thu, 07 Jun 2018 08:29:20 +0200

r-bioc-biovizbase (1.26.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 09 Nov 2017 10:57:05 +0100

r-bioc-biovizbase (1.24.0-1) unstable; urgency=medium

  * New upstream version
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * Versioned Build-Depends: r-bioc-ensembldb (>= 1.99.13)
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Sun, 22 Oct 2017 09:35:44 +0200

r-bioc-biovizbase (1.22.0-2) unstable; urgency=medium

  * Add missing test dependency: r-cran-foreign, r-cran-nnet
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Sun, 15 Jan 2017 13:13:26 +0100

r-bioc-biovizbase (1.22.0-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Generic BioConductor homepage

 -- Andreas Tille <tille@debian.org>  Thu, 27 Oct 2016 15:36:22 +0200

r-bioc-biovizbase (1.20.0-1) unstable; urgency=medium

  * New upstream version
  * New (Build-)Depends: r-bioc-ensembldb

 -- Andreas Tille <tille@debian.org>  Wed, 11 May 2016 15:19:56 +0200

r-bioc-biovizbase (1.18.0-2) unstable; urgency=medium

  * Fix test suite
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Thu, 28 Apr 2016 11:10:06 +0200

r-bioc-biovizbase (1.18.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 06 Nov 2015 13:52:34 +0100

r-bioc-biovizbase (1.16.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 28 Jun 2015 15:02:49 +0200

r-bioc-biovizbase (1.14.0-1) experimental; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * Adapted dependencies

 -- Andreas Tille <tille@debian.org>  Tue, 21 Oct 2014 07:00:24 +0200

r-bioc-biovizbase (1.12.3-1) unstable; urgency=medium

  * New upstream version
  * New (build-)dependencies: r-cran-foreign, r-cran-nnet
  * Add autopkgtest based on vignettes

 -- Andreas Tille <tille@debian.org>  Wed, 17 Sep 2014 11:27:37 +0200

r-bioc-biovizbase (1.12.1-1) unstable; urgency=medium

  * New upstream release
    Closes: #753217
  * Add new (Build-)Dependencies: r-bioc-genomicalignments,
    r-bioc-variantannotation

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jun 2014 13:38:53 +0200

r-bioc-biovizbase (1.10.7-1) unstable; urgency=low

  * Initial release (closes: #733527)

 -- Andreas Tille <tille@debian.org>  Sun, 29 Dec 2013 18:56:22 +0100
