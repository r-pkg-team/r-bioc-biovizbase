Package: biovizBase
Version: 1.54.0
Title: Basic graphic utilities for visualization of genomic data.
Description: The biovizBase package is designed to provide a set of
        utilities, color schemes and conventions for genomic data. It
        serves as the base for various high-level packages for
        biological data visualization. This saves development effort
        and encourages consistency.
Authors@R: c(person("Tengfei", "Yin", role=c("aut"),
                 email="yintengfei@gmail.com"),
          person("Michael", "Lawrence", role=c("aut", "ths", "cre"),
                 email="michafla@gene.com"),
          person("Dianne", "Cook", role=c("aut", "ths")),
          person("Johannes", "Rainer", role="ctb"))
Depends: R (>= 3.5.0), methods
Imports: grDevices, stats, scales, Hmisc, RColorBrewer, dichromat,
        BiocGenerics, S4Vectors (>= 0.23.19), IRanges (>= 1.99.28),
        GenomeInfoDb (>= 1.5.14), GenomicRanges (>= 1.23.21),
        SummarizedExperiment, Biostrings (>= 2.33.11), Rsamtools (>=
        1.17.28), GenomicAlignments (>= 1.1.16), GenomicFeatures (>=
        1.21.19), AnnotationDbi, VariantAnnotation (>= 1.11.4),
        ensembldb (>= 1.99.13), AnnotationFilter (>= 0.99.8), rlang
Suggests: BSgenome.Hsapiens.UCSC.hg19,
        TxDb.Hsapiens.UCSC.hg19.knownGene, BSgenome, rtracklayer,
        EnsDb.Hsapiens.v75, RUnit
License: Artistic-2.0
LazyLoad: Yes
LazyData: Yes
Collate: utils.R color.R AllGenerics.R crunch-method.R mold-method.R
        addStepping-method.R getFragLength-method.R
        shrinkageFun-method.R maxGap-method.R spliceSummary-method.R
        ideogram.R pileup.R coverage.R labs.R original.R transform.R
        facets-method.R aes.R scale.R zzz.R biovizBase-package.R
biocViews: Infrastructure, Visualization, Preprocessing
git_url: https://git.bioconductor.org/packages/biovizBase
git_branch: RELEASE_3_20
git_last_commit: 6735c2d
git_last_commit_date: 2024-10-29
Repository: Bioconductor 3.20
Date/Publication: 2024-10-29
NeedsCompilation: yes
Packaged: 2024-10-29 23:46:42 UTC; biocbuild
Author: Tengfei Yin [aut],
  Michael Lawrence [aut, ths, cre],
  Dianne Cook [aut, ths],
  Johannes Rainer [ctb]
Maintainer: Michael Lawrence <michafla@gene.com>
